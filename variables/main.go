package main

import "fmt"

func main() {
	// var name string = "Dara"
	// var sureName = "Neb"

	var (
		age    = 30
		gender = "Male"
		height = 1.75
		weight = 75.0
	)

	isStudent := true

	fmt.Println("Hello Variable!!!", age, gender, height, weight, isStudent)
}
