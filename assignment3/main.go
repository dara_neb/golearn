package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	args := []string{}
	args = os.Args
	filename := args[1]

	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	data := make([]byte, 100)
	count, err := file.Read(data)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Read %d bytes: %q\n", count, string(data))
}
