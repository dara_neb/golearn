package main

import "fmt"

type person struct {
	firstName string
	lastName  string
	contact   contactInfo
}

type contactInfo struct {
	email   string
	zipCode int
}

func main() {
	// alex := person{"Alex", "Anderson"}
	// fmt.Println(alex)

	// var dara person
	// fmt.Println(dara)
	// if dara.firstName == "" {
	// 	fmt.Println("Empty")
	// }
	// dara.firstName = "dara"
	// dara.lastName = "neb"

	// fmt.Println(dara)

	jim := person{
		firstName: "Jim",
		lastName:  "Party",
		contact: contactInfo{
			email:   "jim@gmail.com",
			zipCode: 94000,
		},
	}
	// jimPointer := &jim
	// jimPointer.updateName("Dara")
	jim.updateName("Dara")
	jim.print()

}

func (p person) print() {
	fmt.Printf("%+v", p)
}

func (p *person) updateName(newFirstName string) {
	(*p).firstName = newFirstName
}
